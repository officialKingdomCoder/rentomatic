import json
import uuid

from rentomatic.serializers import room_json_serializer
from rentomatic.domain.room import Room


def test_serialize_domain_room():
    code = uuid.uuid4()
    size = 200
    price = 10
    longitude = -0.09998975
    latitude = 51.75436293

    room = Room(code, size, price, longitude, latitude)

    expected_json = """
        {{
        "code": "{code}",
        "size": {size},
        "price": {price},
        "longitude": {longitude},
        "latitude": {latitude}
        }}
        """.format(code=code, size=size, price=price, longitude=longitude, latitude=latitude)

    room_json = json.dumps(room, cls=room_json_serializer.RoomJsonEncoder)

    assert json.loads(room_json) == json.loads(expected_json)
