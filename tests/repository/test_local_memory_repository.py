import pytest

from rentomatic.domain.room import Room
from rentomatic.repositories.local_memory_repository import LocalMemoryRepository


@pytest.fixture
def all_room_details():
    return [
        {
            'code': 'f853578c-fc0f-4e65-81b8-566c5dffa35a',
            'size': 215,
            'price': 39,
            'longitude': -0.09998975,
            'latitude': 51.75436293,
        },
        {
            'code': 'fe2c3195-aeff-487a-a08f-e0bdc0ec6e9a',
            'size': 405,
            'price': 66,
            'longitude': 0.18228006,
            'latitude': 51.74640997,
        },
        {
            'code': '913694c6-435a-4366-ba0d-da5334a611b2',
            'size': 56,
            'price': 60,
            'longitude': 0.27891577,
            'latitude': 51.45994069,
        },
        {
            'code': 'eed76e77-55c1-41ce-985d-ca49bf6c0585',
            'size': 93,
            'price': 48,
            'longitude': 0.33894476,
            'latitude': 51.39916678,
        }
    ]


def test_repository_list_without_parameters(all_room_details):
    repository = LocalMemoryRepository(all_room_details)
    rooms = [Room.init_from_dictionary(details_for_one_room) for details_for_one_room in all_room_details]
    assert repository.list() == rooms
