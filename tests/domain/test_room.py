import uuid
from rentomatic.domain.room import Room


def test_room_model_init():
    code = uuid.uuid4()
    size = 200
    price = 10
    longitude = -0.09998975
    latitude = 51.75436293

    room = Room(code, size, price, longitude, latitude)

    assert room.code == code
    assert room.size == size
    assert room.price == price
    assert room.longitude == longitude
    assert room.latitude == latitude


def test_initialize_room_model_from_dictionary():
    code = uuid.uuid4()
    size = 200
    price = 10
    longitude = -0.09998975
    latitude = 51.75436293

    room_details = {
        'code': code,
        'size': size,
        'price': price,
        'longitude': longitude,
        'latitude': latitude
    }

    room = Room.init_from_dictionary(room_details)

    assert room.code == code
    assert room.size == size
    assert room.price == price
    assert room.longitude == longitude
    assert room.latitude == latitude


def test_create_dictionary_of_details_from_room_model():
    code = uuid.uuid4()
    size = 200
    price = 10
    longitude = -0.09998975
    latitude = 51.75436293

    sent_room_details = {
        'code': code,
        'size': size,
        'price': price,
        'longitude': longitude,
        'latitude': latitude
    }

    room = Room.init_from_dictionary(sent_room_details)
    received_room_details = room.generate_room_details_dictionary()

    assert sent_room_details == received_room_details


def test_room_model_comparison():
    code = uuid.uuid4()
    size = 200
    price = 10
    longitude = -0.09998975
    latitude = 51.75436293

    room_details = {
        'code': code,
        'size': size,
        'price': price,
        'longitude': longitude,
        'latitude': latitude
    }

    first_room = Room.init_from_dictionary(room_details)
    second_room = Room.init_from_dictionary(room_details)

    assert first_room == second_room
