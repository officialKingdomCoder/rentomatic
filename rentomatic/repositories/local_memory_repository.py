from rentomatic.domain.room import Room


class LocalMemoryRepository:
    def __init__(self, data):
        self.data = data

    def list(self):
        return [Room.init_from_dictionary(details_for_one_room) for details_for_one_room in self.data]
