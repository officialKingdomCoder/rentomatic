import collections


class InvalidRequestObject:
    def __init__(self):
        self.errors = []

    def add_error(self, parameter, message):
        self.errors.append({'parameter': parameter, 'message': message})

    def has_errors(self):
        return len(self.errors) > 0

    def __bool__(self):
        return False


class ValidRequestObject:
    @classmethod
    def init_from_dictionary(cls, initialization_details):
        raise NotImplementedError

    def __bool__(self):
        return True


class RoomListRequestObject(ValidRequestObject):
    accepted_filters = ['code_eq', 'price_eq', 'price_lower_threshold', 'price_upper_threshold']

    def __init__(self, filters=None):
        self.filters = filters

    @classmethod
    def init_from_dictionary(cls, initialization_details):
        invalid_request = InvalidRequestObject()

        if 'filters' in initialization_details:
            if not isinstance(initialization_details['filters'], collections.Mapping):
                invalid_request.add_error('filters', 'Is not iterable')
                return invalid_request

            for key, value in initialization_details['filters'].items():
                if key not in cls.accepted_filters:
                    invalid_request.add_error(
                        'filters',
                        'key {} cannot be used'.format(key)
                    )

        if invalid_request.has_errors():
            return invalid_request

        return cls(filters=initialization_details.get('filters', None))

    def __bool__(self):
        return True
