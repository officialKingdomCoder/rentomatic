from rentomatic.response_objects.response_objects import ResponseSuccess


class RoomListUseCase:
    def __init__(self, repository):
        self.repository = repository

    def execute(self, request):
        rooms = self.repository.list()
        return ResponseSuccess(rooms)
