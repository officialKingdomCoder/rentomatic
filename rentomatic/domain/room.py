class Room:
    def __init__(self, code, size, price, longitude, latitude):
        self.code = code
        self.size = size
        self.price = price
        self.longitude = longitude
        self.latitude = latitude

    @classmethod
    def init_from_dictionary(cls, room_details):
        return cls(
            code=room_details['code'],
            size=room_details['size'],
            price=room_details['price'],
            longitude=room_details['longitude'],
            latitude=room_details['latitude']
        )

    def generate_room_details_dictionary(self):
        room_details = {
            'code': self.code,
            'size': self.size,
            'price': self.price,
            'longitude': self.longitude,
            'latitude': self.latitude
        }
        return room_details

    def __eq__(self, room_to_compare):
        return self.generate_room_details_dictionary() == room_to_compare.generate_room_details_dictionary()
